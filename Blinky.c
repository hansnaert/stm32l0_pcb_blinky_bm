/*----------------------------------------------------------------------------
 * Name:    Blinky.c
 * Purpose: LED Flasher
 *----------------------------------------------------------------------------
 * This file is part of the uVision/ARM development tools.
 * This software may only be used under the terms of a valid, current,
 * end user licence from KEIL for a compatible version of KEIL software
 * development tools. Nothing else gives you the right to use this software.
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 * Copyright (c) 2015 -2015 Keil - An ARM Company. All rights reserved.
 *----------------------------------------------------------------------------*/

#include <stdio.h>

#include "stm32l0xx.h"                  // Device header


extern int stdout_init (void);

volatile uint32_t msTicks;                            /* counts 1ms timeTicks */
/*----------------------------------------------------------------------------
 * SysTick_Handler:
 *----------------------------------------------------------------------------*/
void SysTick_Handler(void) {
  msTicks++;
}

/*----------------------------------------------------------------------------
 * Delay: delays a number of Systicks
 *----------------------------------------------------------------------------*/
void Delay (uint32_t dlyTicks) {
  uint32_t curTicks;

  curTicks = msTicks;
  while ((msTicks - curTicks) < dlyTicks) { __NOP(); }
}

/*----------------------------------------------------------------------------
 * SystemCoreClockConfigure: configure SystemCoreClock using HSI
                             (HSE is not populated on Nucleo board)
 *----------------------------------------------------------------------------*/
void SystemCoreClockConfigure(void) {

  RCC->CR |= ((uint32_t)RCC_CR_HSION);                     /* Enable HSI */
  while ((RCC->CR & RCC_CR_HSIRDY) == 0);                  /* Wait for HSI Ready */

  RCC->CFGR = RCC_CFGR_SW_HSI;                             /* HSI is system clock */
  while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI);  /* Wait for HSI used as system clock */

  // PLL configuration: PLLCLK = (HSI * 6)/3 = 32 MHz */
  RCC->CFGR &= ~(RCC_CFGR_PLLSRC |
                 RCC_CFGR_PLLMUL |
                 RCC_CFGR_PLLDIV  );
  RCC->CFGR |=  (RCC_CFGR_PLLSRC_HSI |
                 RCC_CFGR_PLLMUL4    |
                 RCC_CFGR_PLLDIV2     );

  FLASH->ACR |= FLASH_ACR_PRFTEN;                          /* Enable Prefetch Buffer */
  FLASH->ACR |= FLASH_ACR_LATENCY;                         /* Flash 1 wait state */

  RCC->APB1ENR |= RCC_APB1ENR_PWREN;                       /* Enable the PWR APB1 Clock */
  PWR->CR = PWR_CR_VOS_0;                                  /* Select the Voltage Range 1 (1.8V) */
  while((PWR->CSR & PWR_CSR_VOSF) != 0);                   /* Wait for Voltage Regulator Ready */

  RCC->CFGR |= RCC_CFGR_HPRE_DIV1;                         /* HCLK = SYSCLK */
  RCC->CFGR |= RCC_CFGR_PPRE1_DIV1;                        /* PCLK1 = HCLK */
  RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;                        /* PCLK2 = HCLK */

  RCC->CR &= ~RCC_CR_PLLON;                                /* Disable PLL */

  RCC->CR |= RCC_CR_PLLON;                                 /* Enable PLL */
  while((RCC->CR & RCC_CR_PLLRDY) == 0) __NOP();           /* Wait till PLL is ready */

  RCC->CFGR &= ~RCC_CFGR_SW;                               /* Select PLL as system clock source */
  RCC->CFGR |=  RCC_CFGR_SW_PLL;
  while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL);  /* Wait till PLL is system clock src */
}

/**
  * Brief   This function enables the peripheral clocks on GPIO port A and B,
  *         configures GPIO PA0 in input mode pull-down for the push-button pin,
  *         configures GPIO PB4 in output mode for the Green LED pin,
  *         configures GPIO PA5 in output mode for the Red LED pin,
  * Param   None
  * Retval  None
  */
__INLINE void  ConfigureGPIO(void)
{  
  /* (1) Enable the peripheral clock of GPIOB and GPIOD */
  /* (2) Select output mode (01) on GPIOB pin 3  
         and output mode (01) on GPIOB pin 4 */
  /* (3) Select input mode (00) on GPIOD pin 2 */
  /* (4) Select Pull-up (01) for PD2 */
  RCC->IOPENR |= RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIODEN; /* (1) */  
  GPIOB->MODER = (GPIOB->MODER & ~(GPIO_MODER_MODE3 | GPIO_MODER_MODE4)) 
               | (GPIO_MODER_MODE3_0 | GPIO_MODER_MODE4_0); /* (2) */  
  GPIOD->MODER = (GPIOD->MODER & ~(GPIO_MODER_MODE2));/* (3) */ 
  GPIOD->PUPDR = (GPIOD->PUPDR & ~(GPIO_PUPDR_PUPD2_0)); /* (4) */
}


/*----------------------------------------------------------------------------
 * main: blink LED and check button state
 *----------------------------------------------------------------------------*/
int main (void) {
  SystemCoreClockConfigure();                              /* configure HSI as System Clock */
  SystemCoreClockUpdate();

	ConfigureGPIO();
  stdout_init();                                           /* Initializ Serial interface */

  SysTick_Config(SystemCoreClock / 1000);                  /* SysTick 1 msec interrupts */

  for (;;) {   
		GPIOB->BSRR = 0x1<<3;																	/* Turn specified LED on */
    Delay(500);                                           /* Wait 500ms */
    while (!(GPIOD->IDR & (1 << 2)));                 		/* Wait while holding USER button */
    GPIOB->BRR = 0x1 <<3; 																/* Turn specified LED off */
    Delay(500);                                           /* Wait 500ms */
    
    printf ("Hello World\n\r");
  }

}
